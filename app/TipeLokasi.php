<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeLokasi extends Model
{
    //
   protected $table = 'in_tipelokasi';
   protected $primaryKey = 'idtipelokasi';
   protected $keyType = 'string';
   public $timestamps = false;

   public $fillable = [
      'idtipelokasi', 'keterangan', 'nourut',
   ];

}
