<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fasilitas;

class Fasilitas extends Model
{
    //
   protected $table = 'in_barang';
   protected $primaryKey = 'idbarang';
   protected $keyType = 'string';

   public $fillable = [
      'idbarang', 'namabarang', 'tipebarang', 'idsatuan',
   ];

   public $timestamps = false;

   // public function gedung()
   // {
   //    return $this->hasOne()
   // }
}
