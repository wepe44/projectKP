<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    protected $table          = 'in_lokasi';
    protected $primaryKey     = 'idlokasi';
    protected $keyType        = 'string';
    public $timestamps        = false;

    protected $fillable = [
      'idlokasi', 'namalokasi', 'idtipelokasi', 'idgedung', 'luas', 'kapasitas',
       'jumlahkursi', 'deskripsi', 'penanggungnip', 'penanggungnama', 'penanggungnip2',
       'penanggungnama2', 't_userid', 't_updatetime', 't_ipaddress', 'jenislokasi',
       'panjang', 'lebar', 'tinggi', 'lantai', 'kondisi', 'jenjang', 'fungsiruang',
       'interkom',
   ];

   public function getGedungAttribute()
   {
     $id = $this->idgedung;
     $name = Gedung::find($id)->namagedung;
     return $name;
   }

   public function gedung()
   {
      return $this->hasMany('App\Gedung','idgedung');
   }
}
