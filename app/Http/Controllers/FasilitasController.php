<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fasilitas;

class FasilitasController extends Controller
{
    //
    public function list_Fasilitas()
    {
      $data = Fasilitas::all();
      return view('backend.list_fasilitas',['fasilitas' => $data]);
   }

   public function add_fasilitas(Request $request)
   {
      // $rules = [
      //   'id' => 'required|unique:in_fasilitas,id_fasilitas',
      // ];
      //
      // $customMessages = [
      //   'required'   => 'The :attribute field can not be blank.',
      //   'unique'     => ':attribute tidak boleh sama',
      // ];

      //$this->validate($request, $rules, $customMessages);

      $fasilitas = new Fasilitas;
      $fasilitas->idbarang    = $request->idbarang;
      $fasilitas->namabarang  = $request->namabarang;
      $fasilitas->tipebarang  = $request->tipebarang;
      $fasilitas->idsatuan    = $request->idsatuan;

      $fasilitas->save();
      $request->session()->flash('success','Data Barang baru berhasil di tambahkan');
      return redirect()->route('list_fasilitas');
   }

   public function update_fasilitas(Request $request)
   {
     $fasilitas = Fasilitas::find($request->idbarang);
     $fasilitas->namabarang  = $request->namabarang;
     $fasilitas->tipebarang  = $request->tipebarang;
     $fasilitas->idsatuan    = $request->idsatuan;

     $fasilitas->save();
     $request->session()->flash('success', 'Barang berhasil di update');
     return redirect()->route('list_fasilitas');
   }

   public function edit_fasilitas($id)
   {

     $all_data = Fasilitas::find($id);
     return view('backend.edit_fasilitas', ['fasilitas' => $all_data]);
   }
}
