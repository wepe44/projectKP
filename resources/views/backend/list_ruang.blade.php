@extends('backend.layouts.app')

@section('add_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('add_js')
   <!-- DataTables -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
@endsection

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>List Lokasi</h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('success'))
      <div class="callout callout-success">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('success') }}
      </div>
    @endif

          <!-- Default box -->
          <div class="box box-info">
            <div class="box-body" style="overflow-x: scroll;">
              <table id="tableRoom" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Tipe</th>
                  <th>Gedung</th>
                  <th>Panjang(m)</th>
                  <th>Lebar(m)</th>
                  <th>Tinggi(m)</th>
                  <th>Lantai</th>
                  <th>Luas(m<sup>2</sup>)</th>
                  <th>Kondisi</th>
                  <th class="text-center no-sort">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($Room) > 0)
                  @foreach ($Room as $key => $value)
                    <tr>
                      <td>{{ $value->idlokasi }}</td>
                      <td>{{ $value->namalokasi }}</td>
                      <td>{{ $value->idtipelokasi }}</td>
                      <td>{{ $value->gedung }}</td>
                      <td>{{ $value->panjang }}</td>
                      <td>{{ $value->lebar }}</td>
                      <td>{{ $value->tinggi }}</td>
                      <td>{{ $value->lantai }}</td>
                      <td>{{ $value->Luas }}</td>
                     @if($value->kondisi == '0')
                        <td>Buruk</td>
                     @elseif($value-> kondisi == null)
                        <td></td>
                     @elseif($value->kondisi == '1')
                        <td>Baik</td>
                     @endif

                      <td class="text-center">
                        <div class="show-inline-flex">
                           <a class="btn btn-xs" style="background-color: #D2691E; color: #fff;" href="{{ route('detail_ruang', ['id' => $value->idlokasi ]) }}"><i class="fa fa-pencil"></i> Detail</a>
                           <form id="form-edit" action="{{ route('edit_ruang') }}" method="post">
                              <input type="hidden" name="id" value="{{ $value->idlokasi }}">
                              {{ csrf_field() }}
                              <button class="btn btn-xs btn-primary">
                                <i class="fa fa-edit"></i> Edit
                              </button>
                           </form>
                           <form id="form-delete" action="{{ route('list_ruang') }}" method="post">
                              <input type="hidden" name="id" value="{{ $value->idlokasi }}">
                              {{ csrf_field() }}
                              <button class="btn btn-xs bg-navy">
                                <i class="fa fa-trash"></i> Delete
                              </button>
                           </form>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>

          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection



@push('add_script')
<script>
  $(function () {
    $("#tableRoom").DataTable();
   //  $('#tableRoom').DataTable({
   //    'paging'      : true,
   //    'lengthChange': true,
   //    'searching'   : true,
   //    'ordering'    : true,
   //    'info'        : false,
   //    'autoWidth'   : true
   // });
  });
</script>
@endpush
