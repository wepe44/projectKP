@extends('backend.layouts.app')

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Ruang </h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('error'))
      <div class="callout callout-danger">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('error') }}
      </div>
      {{ Session::forget('error') }}
    @endif

          <!-- Default box -->
         <div class="box box-info">
              <form role="form" method="post" action="{{ route('edit_ruang.update') }}">
                {{ csrf_field() }}
                <input type="hidden" value="{{ $ruang->idlokasi }}" name="idlokasi"/>
                <div class="box-body">
                  <div class="row">
                    {{-- LEFT SIDE --}}
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Kode Unit</label>
                           <select class="form-control" id="selectUnit" required name="idunit">
                              <option value="" disabled>Select Kode Unit</option>
                              @if(count($unit) > 0)
                                 @foreach ($unit as $key => $value)
                                    @if($value->kodeunit == $ruang->idunit)
                                       <option value="{{ $value->kodeunit }}" selected>{{ $value->namaunit }}</option>
                                    @else
                                       <option value="{{ $value->kodeunit }}">{{ $value->namaunit }}</option>
                                    @endif
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Kode Ruang</label>
                           <input type="text" class="form-control" name="" required placeholder="Kode Ruang" value="{{ $ruang->idlokasi }}">
                        </div>
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Nama Ruang</label>
                           <input type="text" class="form-control" name="namaruang" required placeholder="Nama Ruang" value="{{ $ruang->namalokasi }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Tipe Ruang</label>
                           <select class="form-control" id="selectTipeLokasi" required name="idtipelokasi">
                              <option value="" disabled>Select Tipe Ruang</option>
                              @if(count($tipelokasi) > 0)
                                 @foreach ($tipelokasi as $key => $value)
                                    @if($value->idtipelokasi == $ruang->idunit)
                                       <option value="{{ $value->idtipelokasi }}" selected>{{ $value->keterangan }}</option>
                                    @else
                                       <option value="{{ $value->idtipelokasi }}">{{ $value->keterangan }}</option>
                                    @endif
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Gedung</label>
                           <select class="form-control" id="selectGedung" required name="idgedung">
                              <option value="" selected disabled>Select Nama Gedung</option>
                              @if(count($gedung) > 0)
                                 @foreach ($gedung as $key => $value)
                                    @if($value->idgedung == $ruang->idgedung)
                                       <option value="{{ $value->idgedung }}" selected>{{ $value->namagedung }}</option>
                                    @else
                                       <option value="{{ $value->idgedung }}">{{ $value->namagedung }}</option>
                                    @endif
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Panjang</label>
                           <input type="text" class="form-control" id="e1" name="panjang" placeholder="Panjang" value="{{ $ruang->panjang }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"> Lebar</label>
                           <input type="text" class="form-control" id="e1" name="lebar" placeholder="Lebar" value="{{ $ruang->lebar }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"> Tinggi</label>
                           <input type="text" class="form-control" id="e1" name="tinggi" placeholder="Tinggi" value="{{ $ruang->tinggi }}">
                        </div>
                     </div>

                    {{-- RIGHT SIDE --}}
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Lantai</label>
                           <input type="text" class="form-control" id="e1" name="lantai" placeholder="Lantai" value="{{ $ruang->lantai }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Kapasitas</label>
                           <input type="text" class="form-control" id="e1" name="kapasitas" placeholder="Kapasitas Ruangan" value="{{ $ruang->kapasitas }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Luas</label>
                           <input type="text" class="form-control" id="e1" name="luas" placeholder="Luas Ruangan" value="{{ $ruang->luas }}">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Kondisi</label>
                           <select class="form-control" id="selectKondisi" required name="kondisi">
                              <option value="" selected disabled>Select Kondisi</option>
                              @if($ruang->kondisi == 1)
                                 <option value="1" selected>Baik</option>
                                 <option value="0">Buruk</option>
                              @elseif($ruang->kondisi == 0)
                                 <option value="1">Baik</option>
                                 <option value="0" selected>Buruk</option>
                              @else
                                 <option value="1">Buruk</option>
                                 <option value="0">Buruk</option>
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Jenjang</label>
                           <select class="form-control" id="selectJenjang" required name="jenjang">
                              <option value="" selected disabled>Select Jenjang Pendidikan</option>
                              <option value="D3" {{ $ruang->jenjang === "D3" ? "selected" : "" }} >D3</option>
                              <option value="S1" {{ $ruang->jenjang === "S1" ? "selected" : "" }} >S1</option>
                              <option value="S2" {{ $ruang->jenjang === "S2" ? "selected" : "" }} >S2</option>
                              <option value="S3" {{ $ruang->jenjang === "S3" ? "selected" : "" }} >S3</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Fungsi Ruang</label>
                           <textarea class="form-control" name="fungsiruang" placeholder="Fungsi Ruang">{{ $ruang->fungsiruang }}</textarea>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Interkom</label>
                           <textarea class="form-control" name="interkom" placeholder="ext Interkom">{{ $ruang->interkom }}</textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </form>
         </div>
         <!-- /.box -->
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
    $('#selectJenjang').select2({
      placeholder: "Select Jenjang Pendidikan",
      allowClear: true,
      width: '100%'
    });
    $('#selectKondisi').select2({
      placeholder: "Select Kondisi Gedung",
      allowClear: true,
      width: '100%'
    });
    $('#selectUnit').select2({
      placeholder: "Select Kode Unit",
      allowClear: true,
      width: '100%'
    });
    $('#selectGedung').select2({
      placeholder: "Select Nama Gedung",
      allowClear: true,
      width: '100%'
    });
    $('#selectTipeLokasi').select2({
     placeholder: "Select Tipe Ruang",
     allowClear: true,
     width: '100%'
  });
  //   $('#collects').select2({
  //     placeholder: "Select Collection",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //   $('#categories_sl').select2({
  //     placeholder: "None",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //
  //   $('#examplr').change(function(e){
  //     var formData = {
  //       prms: $(this).val()
  //     }
  //     e.preventDefault();
  //     $.ajax({
  //       type: 'post',
  //       url: '/staff/collection/find',
  //       data: formData,
  //       dataType: 'json',
  //       success: function (data) {
  //         $('#collects').html('');
  //         $('#collects').select2({
  //           placeholder: "Select Collection",
  //           allowClear: true,
  //           data: data
  //         });
  //         $('#categories_sl').html('');
  //         $('#categories_sl').select2({
  //           placeholder: "None",
  //           allowClear: true
  //         });
  //       }
  //     });
  //   });
  //
  //   $('#collects').change(function(e){
  //     e.preventDefault();
  //     var val = $(this).val();
  //     if (val.length > 0 ){
  //       var formData = {
  //         prms: $(this).val()
  //       }
  //       $.ajax({
  //         type: 'post',
  //         url: '/staff/parentcategory/find',
  //         data: formData,
  //         dataType: 'json',
  //         success: function (data) {
  //           $('#categories_sl').html('');
  //           $('#categories_sl').select2({
  //             placeholder: "None",
  //             allowClear: true,
  //             data: data
  //           });
  //         }
  //       });
  //     }
  //   });
  });
</script>
@endpush
