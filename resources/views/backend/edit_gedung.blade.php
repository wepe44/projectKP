@extends('backend.layouts.app')

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Gedung <small>add new gedung</small></h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('error'))
      <div class="callout callout-danger">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('error') }}
      </div>
      {{ Session::forget('error') }}
    @endif

          <!-- Default box -->
          <div class="box box-info">
              <form role="form" method="post" action="{{ route('edit_gedung',['id' => $gedung->idgedung]) }}">
                {{ csrf_field() }}
                <div class="box-body">
                  <div class="row">
                    {{-- LEFT SIDE --}}
                    <div class="col-md-6">
                        <div class="form-group">
                         <label for="examplr"><span style="color:red;">*</span> Kode Gedung</label>
                         <input type="text" class="form-control" name="id" required placeholder="Kode Gedung" value="{{ $gedung->idgedung }}" disabled>
                        </div>
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Nama Gedung</label>
                           <input type="text" class="form-control" name="namagedung" required placeholder="Nama Gedung" value="{{ $gedung->namagedung }}" >
                        </div>
                        <div class="form-group">
                           <label for="collects"> Tahun Bangun</label>
                           <input type="text" class="form-control" name="tahunbangun" placeholder="Tahun Bangun" value="{{ $gedung->tahunbangun }}" >
                        </div>
                        <div id="categoriess" class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Jumlah Lantai</label>
                           <input type="text" class="form-control" name="jumlahlantai" required placeholder="Jumlah Lantai" value="{{ $gedung->jmllantai }}" >
                        </div>
                    </div>

                    {{-- RIGHT SIDE --}}
                    <div class="col-md-6">
                       <div class="form-group">
                         <label for="collects"><span style="color:red;">*</span> Lokasi</label>
                         <input type="text" class="form-control" id="e1" name="lokasi" required placeholder="Nama Lokasi" value="{{ $gedung->lokasi }}" >
                       </div>
                       <div class="form-group">
                         <label for="collects"><span style="color:red;">*</span> Alamat</label>
                         <input type="text" class="form-control" id="e1" name="alamat" required placeholder="Alamat" value="{{ $gedung->alamat }}" >
                       </div>
                       <div class="form-group">
                         <label for="collects"><span style="color:red;">*</span> No. Urut</label>
                         <input type="text" class="form-control" id="e1" name="nourut" required placeholder="Urutan Bangunan" value="{{ $gedung->nourut }}" >
                       </div>
                    </div>

                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
    $('#examplr').select2({
      placeholder: "Select Permission",
      allowClear: true,
      width: '100%'
    });
  //   $('#collects').select2({
  //     placeholder: "Select Collection",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //   $('#categories_sl').select2({
  //     placeholder: "None",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //
  //   $('#examplr').change(function(e){
  //     var formData = {
  //       prms: $(this).val()
  //     }
  //     e.preventDefault();
  //     $.ajax({
  //       type: 'post',
  //       url: '/staff/collection/find',
  //       data: formData,
  //       dataType: 'json',
  //       success: function (data) {
  //         $('#collects').html('');
  //         $('#collects').select2({
  //           placeholder: "Select Collection",
  //           allowClear: true,
  //           data: data
  //         });
  //         $('#categories_sl').html('');
  //         $('#categories_sl').select2({
  //           placeholder: "None",
  //           allowClear: true
  //         });
  //       }
  //     });
  //   });
  //
  //   $('#collects').change(function(e){
  //     e.preventDefault();
  //     var val = $(this).val();
  //     if (val.length > 0 ){
  //       var formData = {
  //         prms: $(this).val()
  //       }
  //       $.ajax({
  //         type: 'post',
  //         url: '/staff/parentcategory/find',
  //         data: formData,
  //         dataType: 'json',
  //         success: function (data) {
  //           $('#categories_sl').html('');
  //           $('#categories_sl').select2({
  //             placeholder: "None",
  //             allowClear: true,
  //             data: data
  //           });
  //         }
  //       });
  //     }
  //   });
  });
</script>
@endpush
