@extends('backend.layouts.app')

@section('add_css')
<!-- select2 -->
<link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('add_js')
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
@endsection

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Fasilitas <small>add new fasilitas</small></h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('error'))
      <div class="callout callout-danger">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('error') }}
      </div>
      {{ Session::forget('error') }}
    @endif

          <!-- Default box -->
          <div class="box box-info">
              <form role="form" method="post" action="{{ route('add_fasilitas') }}">
                {{ csrf_field() }}
                <div class="box-body">
                  <div class="row">
                    {{-- LEFT SIDE --}}
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="examplr"><span style="color:red;">*</span> Id Barang</label>
                        <input type="text" class="form-control" name="idbarang" required placeholder="Id Barang">
                      </div>
                      <div class="form-group">
                        <label for="collects"><span style="color:red;">*</span> Nama Barang</label>
                        <input type="text" class="form-control" id="e1" name="namabarang" required placeholder="Nama Barang">
                      </div>
                    </div>

                    {{-- RIGHT SIDE --}}
                    <div class="col-md-6">
                       <div class="form-group">
                         <label for="collects"><span style="color:red;">*</span> Tipe Ruang</label>
                         <input type="text" class="form-control" id="e1" name="tiperuang" required placeholder="Tipe Ruang">
                       </div>
                       <div class="form-group">
                         <label for="collects"><span style="color:red;">*</span> Satuan</label>
                         <input type="text" class="form-control" id="e1" name="idsatuan" required placeholder="Id Satuan">
                       </div>
                    </div>

                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
    $('#examplr').select2({
      placeholder: "Select Permission",
      allowClear: true,
      width: '100%'
    });
  //   $('#collects').select2({
  //     placeholder: "Select Collection",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //   $('#categories_sl').select2({
  //     placeholder: "None",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //
  //   $('#examplr').change(function(e){
  //     var formData = {
  //       prms: $(this).val()
  //     }
  //     e.preventDefault();
  //     $.ajax({
  //       type: 'post',
  //       url: '/staff/collection/find',
  //       data: formData,
  //       dataType: 'json',
  //       success: function (data) {
  //         $('#collects').html('');
  //         $('#collects').select2({
  //           placeholder: "Select Collection",
  //           allowClear: true,
  //           data: data
  //         });
  //         $('#categories_sl').html('');
  //         $('#categories_sl').select2({
  //           placeholder: "None",
  //           allowClear: true
  //         });
  //       }
  //     });
  //   });
  //
  //   $('#collects').change(function(e){
  //     e.preventDefault();
  //     var val = $(this).val();
  //     if (val.length > 0 ){
  //       var formData = {
  //         prms: $(this).val()
  //       }
  //       $.ajax({
  //         type: 'post',
  //         url: '/staff/parentcategory/find',
  //         data: formData,
  //         dataType: 'json',
  //         success: function (data) {
  //           $('#categories_sl').html('');
  //           $('#categories_sl').select2({
  //             placeholder: "None",
  //             allowClear: true,
  //             data: data
  //           });
  //         }
  //       });
  //     }
  //   });
  });
</script>
@endpush
