@extends('backend.layouts.app')

@section('add_css')
   <!-- Bootstrap time Picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css') }}">
   <!-- Date Picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('add_js')
   <!-- daterangepicker -->
   <script src="{{ asset('bower_components/moment/min/moment.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
   <!-- datepicker -->
   <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
   <!-- timepicker -->
   <script src="{{ asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
   <script src="{{ asset('backend/dist/js/style.js') }}"></script>
@endsection

@section('add_content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Ruang <small>add new ruang</small></h1>
  </section>

  <!-- Main content -->
  <section class="content">

  {{-- ERROR HERE   --}}
  @if(count($errors) > 0)
    <div class="callout callout-danger">
    <h4><i class="fa fa-warning"></i> Note:</h4>
    @foreach ($errors->all() as $error)
    {{ $error }} <br />
    @endforeach
    </div>
  @elseif (Session::has('error'))
    <div class="callout callout-danger">
      <h4><i class="fa fa-check"></i> Note:</h4>
      {{ Session::get('error') }}
    </div>
    {{ Session::forget('error') }}
  @endif

        <!-- Default box -->
       <div class="box box-info">
            <form role="form" method="post" action="{{ route('add_transaksi') }}">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="row">
                  {{-- LEFT SIDE --}}
                   <div class="col-md-6">
                      <div class="form-group">
                         <label for="examplr"> Nama Kegiatan </label>
                         <input type="text" class="form-control" name="namakegiatan" required placeholder="Nama Kegiatan">
                      </div>
                      <div class="form-group">
                        <label for="examplr"> Tanggal Kegiatan </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="tanggalkegiatan" name="tanggalkegiatan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="examplr"> Waktu Mulai Kegiatan </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-left" id="tanggalmulai" name="tanggalmulai">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="examplr"> Jam Mulai </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="jamMulai" name="jammulai">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="examplr"> Waktu Selesai Kegiatan </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-left" id="tanggalselesai" name="tanggalmulai">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="examplr"> Jam Selesai </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control timepicker" id="jamSelesai" name="jammulai">
                        </div>
                      </div>
                   </div>

                  {{-- RIGHT SIDE --}}
                   <div class="col-md-6">
                      <div class="form-group">
                         <label for="collects"> Interkom</label>
                         <textarea class="form-control" name="interkom" placeholder="ext Interkom"></textarea>
                      </div>
                   </div>
                </div>
             </div>
             <!-- /.box-body -->
             <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
             </div>
          </form>
       </div>
       <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>

$(function () {
   //Date picker
   $('#tanggalkegiatan').datepicker({
      autoclose: true,
      showInput: false,
   })
   $('#tanggalmulai').datepicker({
      autoclose: true,
      showInput: false,
   })
   //Timepicker
   $('#jamMulai').timepicker({
      showInputs: false
   })
   //Date picker
   $('#tanggalselesai').datepicker({
      autoclose: true,
      showInput: false,
   })
   //Timepicker
   $('#jamSelesai').timepicker({
      showInputs: false
   })

});
</script>
@endpush
