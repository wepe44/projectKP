@extends('backend.layouts.app')

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Ruang <small>add new ruang</small></h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('error'))
      <div class="callout callout-danger">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('error') }}
      </div>
      {{ Session::forget('error') }}
    @endif

          <!-- Default box -->
          <div class="box box-info">
            <div class="box-body">
              <div class="row">
                <table id="tableRoom" class="table table-bordered table-striped">
                  <tbody>
                    @if(count($Room) > 0)
                      @foreach ($Room as $key => $value)
                      <tr>
                        <td>Kode Ruang</td>
                        <td>:</td>
                        <td>{{ $value->idlokasi }}</td>
                      </tr>
                      <tr>
                        <td>Nama Ruang</td>
                        <td>:</td>
                        <td>{{ $value->namalokasi }}</td>
                      </tr>
                      <tr>
                        <td>Tipe Lokasi</td>
                        <td>:</td>
                        <td>{{ $value->idtipelokasi }}</td>
                      </tr>
                      <tr>
                        <td>Gedung</td>
                        <td>:</td>
                        <td>{{ $value->gedung }}</td>
                      </tr>
                      <tr>
                        <td>Panjang</td>
                        <td>:</td>
                        <td>{{ $value->panjang }}</td>
                      </tr>
                      <tr>
                        <td>Lebar</td>
                        <td>:</td>
                        <td>{{ $value->lebar }}</td>
                      </tr>
                      <tr>
                        <td>Tinggi</td>
                        <td>:</td>
                        <td>{{ $value->tinggi }}</td>
                      </tr>
                      <tr>
                        <td>Lantai</td>
                        <td>:</td>
                        <td>{{ $value->lantai }}</td>
                      </tr>
                      <tr>
                        <td>Kapasitas</td>
                        <td>:</td>
                        <td>{{ $value->kapasitas }}</td>
                      </tr>
                      <tr>
                        <td>Luas</td>
                        <td>:</td>
                        <td>{{ $value->luas }}</td>
                      </tr>
                      <tr>
                        <td>Kondisi</td>
                        <td>:</td>
                        @if($value->kondisi == '0')
                          <td>Buruk</td>
                       @elseif($value-> kondisi == null)
                          <td></td>
                       @elseif($value->kondisi == '1')
                          <td>Baik</td>
                       @endif
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" action="{{ route('list_ruang') }}">Back</button>
            </div>
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
    $('#examplr').select2({
      placeholder: "Select Permission",
      allowClear: true,
      width: '100%'
    });
  //   $('#collects').select2({
  //     placeholder: "Select Collection",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //   $('#categories_sl').select2({
  //     placeholder: "None",
  //     allowClear: true,
  //     width: '100%'
  //   });
  //
  //   $('#examplr').change(function(e){
  //     var formData = {
  //       prms: $(this).val()
  //     }
  //     e.preventDefault();
  //     $.ajax({
  //       type: 'post',
  //       url: '/staff/collection/find',
  //       data: formData,
  //       dataType: 'json',
  //       success: function (data) {
  //         $('#collects').html('');
  //         $('#collects').select2({
  //           placeholder: "Select Collection",
  //           allowClear: true,
  //           data: data
  //         });
  //         $('#categories_sl').html('');
  //         $('#categories_sl').select2({
  //           placeholder: "None",
  //           allowClear: true
  //         });
  //       }
  //     });
  //   });
  //
  //   $('#collects').change(function(e){
  //     e.preventDefault();
  //     var val = $(this).val();
  //     if (val.length > 0 ){
  //       var formData = {
  //         prms: $(this).val()
  //       }
  //       $.ajax({
  //         type: 'post',
  //         url: '/staff/parentcategory/find',
  //         data: formData,
  //         dataType: 'json',
  //         success: function (data) {
  //           $('#categories_sl').html('');
  //           $('#categories_sl').select2({
  //             placeholder: "None",
  //             allowClear: true,
  //             data: data
  //           });
  //         }
  //       });
  //     }
  //   });
  });
</script>
@endpush
