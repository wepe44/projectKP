<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/', 'HomeController@index');
//Route::get('/',function(){ return view('welcome'); });

Route::prefix('admin')->group(function(){
   Route::get('/dashboard', function(){ return view('backend.dashboard'); })->name('dashboard');

   Route::get('/addfasilitas', function(){ return view('backend.input_fasilitas'); })->name('add_fasilitas');
   Route::post('/addfasilitas','FasilitasController@add_Fasilitas');
   Route::get('/listfasilitas','FasilitasController@list_Fasilitas')->name('list_fasilitas');
   Route::get('/editfasilitas/{id}','FasilitasController@edit_fasilitas')->name('edit_fasilitas');
   Route::post('/editfasilitas/{id}','FasilitasController@update_fasilitas');

   Route::get('/addruang', 'RuangController@input_ruang')->name('add_ruang');
   Route::post('/addruang', 'RuangController@add_ruang');
   Route::get('/listruang','RuangController@list_Ruang')->name('list_ruang');
   Route::post('/editruang','RuangController@edit_ruang')->name('edit_ruang');
   Route::post('/editruang/update','RuangController@update_ruang')->name('edit_ruang.update');
   Route::get('/detailruang/{id}','RuangController@detail_ruang')->name('detail_ruang');

   Route::get('/addgedung', function(){ return view('backend.input_gedung'); })->name('add_gedung');
   Route::post('/addgedung', 'GedungController@add_gedung');
   Route::get('/listgedung','GedungController@list_gedung')->name('list_gedung');
   Route::get('/editgedung/{id}','GedungController@edit_gedung')->name('edit_gedung');
   Route::post('/editgedung/{id}','GedungController@update_gedung');

   Route::get('/addtransaksi', function(){ return view('backend.input_transaksi'); })->name('add_transaksi');
   Route::post('/addtransaksi', 'TransaksiController@add_transaksi');
   Route::get('/listtransaksi','TransaksiController@list_transaksi')->name('list_transaksi');
   Route::get('/edittransaksi/{id}','TransaksiController@edit_transaksi')->name('edit_transaksi');
   Route::post('/edittransaksi/{id}','TransaksiController@update_transaksi');

});
