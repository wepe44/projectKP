$(function () {
  $(".select2").select2();

  $('#gender').change(function(e) {
    $(".select2").empty();
    var gender = $(this).val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    e.preventDefault();
    var formData = {
        gender: gender
    }
    $.ajax({
        type: 'post',
        url: '/backend/category/find',
        data: formData,
        dataType: 'json',
        success: function (data) {
          $('.select2').select2({
            data: data,
            placeholder: "None"
          });
        }
    });
  });

  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
  });
  $('.form-horizontal').submit(function(e) {
      var currentForm = this;
      e.preventDefault();
      bootbox.confirm("Are you sure?", function(result) {
          if (result) {
              currentForm.submit();
          }
      });
  });

  $('#gender').trigger('change');

});
